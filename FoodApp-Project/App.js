import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { Home,Restaurant,Login,Register,AboutMe,Like } from './screens';
import {Tabs,Drawwer} from './navigation/index'
import { useFonts } from 'expo-font';
import { icons, images, SIZES, COLORS, FONTS } from './constants'
import AppLoading from "expo-app-loading";
import { useEffect } from 'react';
import { useState } from 'react';
import { getAuth, onAuthStateChanged } from "firebase/auth";
import {
  getApps,
  initializeApp
} from "firebase/app";

const Stack = createStackNavigator()


export default function App() {
  // const [loaded] = useFonts({
  //   "Roboto-Black" : require('./assets/fonts/Roboto-Black.ttf'),
  //   "Roboto-Bold" : require('./assets/fonts/Roboto-Bold.ttf'),
  //   "Roboto-Regular" : require('./assets/fonts/Roboto-Regular.ttf'),
  
  // })
  
  // if(!loaded){
  //   return <AppLoading/>;
  // }
  const firebaseConfig = {
    apiKey: "AIzaSyCNLnaf5UUZMZrHDRPWHLkNwtj5jPiTZ7g",
    authDomain: "fir-auth-e9232.firebaseapp.com",
    projectId: "fir-auth-e9232",
    storageBucket: "fir-auth-e9232.appspot.com",
    messagingSenderId: "484370293536",
    appId: "1:484370293536:web:4a3444b1c7222e56f8ed45"
};

  if (!getApps().length) {
    const app = initializeApp(firebaseConfig);
}

  const [isSignedIn,setIsSignedIn] = useState('')

    useEffect(() =>{
      const auth = getAuth();
      const unsubscribe = onAuthStateChanged(auth, (user) => {
        if (user) {
          const uid = user.uid;
          setIsSignedIn(true)
        } else {
          setIsSignedIn(false)
        }
      });
      return unsubscribe
    },[])

  return (
      <>
      <StatusBar 
        style='grey'
        translucent={false}
        backgroundColor={COLORS.lightGray4}
        />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{  
            headerShown:false
          }}
          initialRouteName={'Login'}
          >
            {!isSignedIn ?(
            <>
            <Stack.Screen name='Login' component={Login} />
            <Stack.Screen name='Register' component={Register} />
            </>
            ) : (  
            <>
            <Stack.Screen name='Home' component={Tabs} />
            <Stack.Screen name ='MyDrawwer' component={Drawwer}/>
            </>
            )} 
        </Stack.Navigator>
      </NavigationContainer>
      </>
  ); 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
