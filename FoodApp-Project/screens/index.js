import Home from './Home'
import Restaurant from './Restaurant'
import Login from './Login'
import Register from './Register'
import AboutMe from './AboutMe'
import Like from './Like'
export {
    Home,
    Restaurant,
    Login,
    Register,
    AboutMe,
    Like
}