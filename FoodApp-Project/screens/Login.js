import { StyleSheet, Text, View,TextInput,Image } from 'react-native'
import React, { useState } from 'react'
import { Ionicons } from '@expo/vector-icons'; 
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { icons, images, SIZES, COLORS, FONTS } from '../constants'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

const Login = () => {
  const navigation=useNavigation()

  const [email,setEmail] = useState('')
  const [password,setPassword] =  useState('')

  const handleSignIn = () => {       
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        navigation.navigate('Home')
        console.log(`Login with ${user.email}`);
      })
      .catch(error => alert(error.message))
    ;
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
          <Image source={icons.sushi} style={styles.logo}/>
          <Text style={styles.logoText}>FoodApp</Text>
      </View>    
      <View style={styles.header1}>
        <Text style={styles.header1Text}>Hello,</Text>
        <Text style={styles.header1Text}>Welcome Back!</Text>
      </View>    
      <View style={styles.textInputView}>
        <View style={styles.textInputViewStyle}>
          <Ionicons name="person" size={20} color={COLORS.darkgray} /> 
          <TextInput style ={styles.textInputSyle}
            placeholder='your email'
            placeholderTextColor={COLORS.darkgray}
            onChangeText={(value) => setEmail(value)}
          />
        </View>
        <View style={styles.textInputViewStyle}>
          <Ionicons name="lock-closed" size={20} color={COLORS.darkgray} />
          <TextInput style ={styles.textInputSyle}
            placeholder='password'
            placeholderTextColor={COLORS.darkgray}
            secureTextEntry
            onChangeText={(value) => setPassword(value)} 
          /> 
        </View>
      </View>  
      <View style={styles.buttonView}>
        <TouchableOpacity  
        onPress={handleSignIn}
        style={styles.buttonStyle}>
          <Text style={StyleSheet.buttonTextStyle}>Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>(navigation.navigate('Register'))} >
          <Text style={{color:COLORS.primary,fontSize:16}}>Sign Up</Text>
        </TouchableOpacity>
      </View>    
      
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  container:{
      flex: 1,
      backgroundColor: COLORS.lightGray4
  },
  header: {
    alignItems:'center',
    backgroundColor:COLORS.lightGray4,
    paddingTop:10
  },
  logo:{
    height:100,
    width:100
  },
  logoText:{
    fontSize:34,
    color:COLORS.primary,
    fontWeight:'bold'
  },
  header1:{
    backgroundColor:COLORS.lightGray4,
    paddingLeft:30,
    marginTop:20
  },
  header1Text:{
    fontSize:34,
    color:COLORS.black,
    fontWeight:'bold'
  },
  textInputView:{
    flex:1,
    backgroundColor:COLORS.lightGray4,
    alignItems:'center',
    marginTop:10,
    justifyContent:'center'
  },
  textInputViewStyle:{
    width:"80%",
    height:40,
    backgroundColor:COLORS.secondary,
    borderRadius:7,
    alignItems:'center',
    padding:5,
    flexDirection:'row',
    margin:5
  },
  textInputSyle:{
    color:COLORS.black,
    fontSize:14,
    padding:5,
    flex:1
  },
  buttonView:{
    flex:1,
    backgroundColor:COLORS.lightGray4,
    paddingBottom:20,
    alignItems:'center'
  },
  buttonTextStyle:{
    paddingVertical:14,
    paddingHorizontal:40,
    fontSize:16
  },
  buttonStyle:{
    backgroundColor:COLORS.primary,
    borderRadius:7,
    margin:10,
    paddingVertical:12,
    paddingHorizontal:40
  },
})

