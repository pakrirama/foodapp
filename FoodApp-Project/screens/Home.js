import { View, Text,SafeAreaView,StyleSheet,TouchableOpacity,Image,FlatList } from 'react-native'
import React from 'react'
import { icons, images, SIZES, COLORS, FONTS,categoryData } from '../constants'
import { getAuth,signOut  } from 'firebase/auth'
import { useState } from 'react'


const Home = ({navigation}) => {

    const handleSignOut = () => {       
        const auth =getAuth()
        .signOut()
        .then(() => console.log('User signed out!'));
        }
    function renderHeader() {
        return (
        // <View syle={StyleSheet.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.headerButton}>
                        <Image
                            source={icons.nearby} 
                            style={styles.headerIcon}
                        />
                    </TouchableOpacity>
                        <View style={styles.headerBar}>
                            <Text style={{fontSize:20,fontWeight:'bold'}}>Lokasi</Text>
                        </View>   
                    <TouchableOpacity style={styles.headerButton}
                    onPress={handleSignOut}
                    >
                        <Image
                            source={icons.basket}
                            resizeMode="contain"
                            style={styles.headerIcon}
                        />
                    </TouchableOpacity>
                </View>
        )
    }

    function onSelectCategory(item) {
        setSelectedCategory(item)
    }

    const [selectedCategory,setSelectedCategory] = useState('')
    const [categories, setCategories] = useState(categoryData)
    
    function renderMainCategories() {
        const renderItem = ({ item }) => {
            return (
                <TouchableOpacity
                    style={{
                        padding: SIZES.padding,
                        paddingBottom: SIZES.padding * 2,
                        backgroundColor: (selectedCategory?.id == item.id) ? COLORS.primary : COLORS.white,
                        borderRadius: SIZES.radius,
                        alignItems: "center",
                        justifyContent: "center",
                        marginRight: SIZES.padding,
                        ...styles.shadow
                    }}
                    onPress={() => onSelectCategory(item)}
                >
                    <View
                        style={{
                            width: 50,
                            height: 50,
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: selectedCategory?.id == item.id ? COLORS.white : COLORS.secondary
                        }}
                    >
                        <Image
                            source={item.icon}
                            resizeMode="contain"
                            style={{
                                width: 40,
                                height: 40
                            }}
                        />
                    </View>

                    <Text
                        style={{
                            marginTop: SIZES.padding,
                            color: (selectedCategory?.id == item.id) ? COLORS.white : COLORS.black,
                            ...FONTS.body5
                        }}
                    >
                        {item.name}
                    </Text>
                </TouchableOpacity>
            )
        }

        return (
            <View style={{ padding: SIZES.padding * 2 }}>
                <Text style={{ ...FONTS.h1 }}>Main</Text>
                <Text style={{ ...FONTS.h1 }}>Categories</Text>

                <FlatList
                    data={categories}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => `${item.id}`}
                    renderItem={renderItem}
                    contentContainerStyle={{ paddingVertical: SIZES.padding * 2 }}
                />
            </View>
        ) 
    }
        
    return (
        <SafeAreaView style={styles.container}>
            {renderHeader()}
            {renderMainCategories()}
        </SafeAreaView>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.lightGray4
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        elevation: 1,
    },
    headerIcon:{
        width: 30,
        height: 30
    },
    header:{
        flexDirection: 'row',
        height: 50,
        marginTop:10,
        justifyContent:'space-between'
    },
    headerButton:{
        width: 50,
        paddingLeft:10,
        justifyContent: 'center'           
    },
    headerBar:{
        width: '50%',
        height: "90%",
        backgroundColor: COLORS.secondary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: SIZES.radius,    
    }
})