import { StyleSheet, Text, View,Image,TouchableOpacity } from 'react-native'
import React from 'react'
import { Ionicons } from '@expo/vector-icons'; 
import { COLORS,images,icons } from '../constants'

const AboutMe = () => {
    return (
        <View style={styles.container}>
            <View style={styles.containerWrapper}>
                <Image source={images.photoAboutMe}
                style={styles.photoAboutMe1}/>
                <View style ={styles.containerWrapper2}>
                    <Text>Hello I'm Muhammad Fakhri Ramadhan</Text>
                    <Text>This is my very first app</Text>
                </View>
                <View style={styles.containerWrapper3}>
                    <Text style={styles.textContainer}>Portofolio</Text>
                    <View style={styles.aboutMeContent}>
                        <TouchableOpacity>
                            <Image source={images.react_native} style={styles.portofolioContent}/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={images.data_science} style={styles.portofolioContent}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.containerWrapper3}>
                    <Text style={styles.textContainer}>Language</Text>
                    <View style={styles.aboutMeContent}>
                        <Image source={images.language} style={styles.portofolioContent}/>
                    </View>
                </View>
                <View style={styles.containerWrapper3}>
                    <Text style={styles.textContainer}>Contact</Text>
                    <View style={styles.aboutMeContent}>
                        <Image source={images.contacts} style={styles.contactContent}/>
                    </View>
                </View>
            </View>
        </View>
    )
}
{/* <Image source={images.photoAboutMe}
    style={styles.photoAboutMe1}
/>  */}

export default AboutMe

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.lightGray4,
    },
    containerWrapper:{
        flex:1,
        alignItems:'center',

    },
    photoAboutMe1:{
        borderRadius:100,
        borderWidth:4,
        borderColor:COLORS.primary,
        width:120,
        height:120,
        margin:25
    },
    containerWrapper2:{
        width:'85%',
        height:100,
        marginVertical:10,
        borderRadius:7,
        alignItems:'center'
    },
    containerWrapper3:{
        backgroundColor:COLORS.secondary,
        width:'85%',
        height:110,
        marginVertical:10,
        borderRadius:7,
        borderWidth:3,
        borderColor:COLORS.primary,
        alignItems:'center'
    },
    textContainer:{
        fontSize:20,
        fontWeight:'bold',
        color:'white',
        margin:5
    },
    portofolioContent:{
        resizeMode:'contain',
        width:135,
        borderRadius:15,
        marginHorizontal:5,
    },
    contactContent:{
        resizeMode:'contain',
        width:300,
        borderRadius:15,
        marginHorizontal:5,
    },
    aboutMeContent:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    }
})