import icons from "./icons";
import images from "./images";
import categoryData from "./dummydata";
import { COLORS, SIZES, FONTS } from "./theme";

export { icons, images,categoryData, COLORS, SIZES, FONTS,};
