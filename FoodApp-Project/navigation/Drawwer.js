import { StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'

import { createDrawerNavigator } from '@react-navigation/drawer';
import { Home,Login,Register,Restaurant,AboutMe,Like} from '../screens'
import { Tabs } from './index';

const Drawer = createDrawerNavigator();

const Drawwer = () => (
    <Drawer.Navigator>
        <Drawer.Screen name='Drawer' component={Tabs} />
        <Drawer.Screen name='AboutMe' component={AboutMe} />
    </Drawer.Navigator>
)

export default Drawwer