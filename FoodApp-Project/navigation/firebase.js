// Import the functions you need from the SDKs you need
import {
    getApps,
    initializeApp
} from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCNLnaf5UUZMZrHDRPWHLkNwtj5jPiTZ7g",
    authDomain: "fir-auth-e9232.firebaseapp.com",
    projectId: "fir-auth-e9232",
    storageBucket: "fir-auth-e9232.appspot.com",
    messagingSenderId: "484370293536",
    appId: "1:484370293536:web:4a3444b1c7222e56f8ed45"
};

// Initialize Firebase
if (!getApps().length) {
    const app = initializeApp(firebaseConfig);
}

console.log(getApps());