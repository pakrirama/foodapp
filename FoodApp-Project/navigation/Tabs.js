import { StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'
import { icons,COLORS } from '../constants'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { Home,Login,Register,Restaurant,AboutMe,Like} from '../screens'


const Tab = createBottomTabNavigator()

const Tabs = () => {
    return (
        <Tab.Navigator
            screenOptions={{  
                headerShown:false,
                tabBarShowLabel:false,
            }}
            >
            <Tab.Screen 
                name='HomeScreen'
                component={Home}
                options ={{
                    tabBarIcon: ({ focused }) => (
                        <Image 
                            source = {icons.cutlery}
                            resizeMode ='contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? COLORS.primary : COLORS.secondary,
                            }}
                        />
                    )
                }}
                />
            <Tab.Screen 
                name='Search'
                component={Restaurant}
                options ={{
                    tabBarIcon: ({ focused }) => (
                        <Image 
                            source = {icons.search}
                            resizeMode ='contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? COLORS.primary : COLORS.secondary
                            }}
                        />
                    )
                }}
                />
                <Tab.Screen 
                name='Like'
                component={Like}
                options ={{
                    tabBarIcon: ({ focused }) => (
                        <Image 
                            source = {icons.like}
                            resizeMode ='contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? COLORS.primary : COLORS.secondary
                            }}
                        />
                    )
                }}
                />
                <Tab.Screen 
                name='AboutMe'
                component={AboutMe}
                options ={{
                    tabBarIcon: ({ focused }) => (
                        <Image 
                            source = {icons.user}
                            resizeMode ='contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? COLORS.primary : COLORS.secondary
                            }}
                        />
                    )
                }}
                />
        </Tab.Navigator>
    )
}

export default Tabs

const styles = StyleSheet.create({
    
})